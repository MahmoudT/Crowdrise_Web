<?php

namespace Crowdrise\IdeesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function someideasAction()
    {
        return $this->render('CrowdriseIdeesBundle:Default:some_ideas.html.twig');
    }
    
    public function allideasAction()
    {
        return $this->render('CrowdriseIdeesBundle:Default:all_ideas.html.twig');
    }

}
